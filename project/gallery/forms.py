# coding: utf-8

import itertools

from django import forms
from django.utils.translation import ugettext_lazy as _
from slugify import slugify

from .models import Gallery, Image


class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ('name', 'description')

    def save(self):
        if self.instance.pk:
            return super(GalleryForm, self).save()

        instance = super(GalleryForm, self).save(commit=False)

        max_length = Gallery._meta.get_field('slug').max_length
        instance.slug = orig = slugify(instance.name, only_ascii=True)[:max_length]

        for x in itertools.count(1):
            if not Gallery.objects.filter(slug=instance.slug).exists():
                break

            instance.slug = "%s-%d" % (orig[:max_length - len(str(x)) - 1], x)

        instance.save()

        return instance


class ImageForm(forms.ModelForm):
    img = forms.ImageField(label=_(u'obrazek'))

    class Meta:
        model = Image
        fields = ('img', 'description',)