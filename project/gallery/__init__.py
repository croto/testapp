# coding: utf-8

from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

from .settings import GALLERY_DIRECTORY_BASE, GALLERY_DIRECTORY

if GALLERY_DIRECTORY_BASE[-1] is not '/':
    raise ImproperlyConfigured(_(u'GALLERY_DIRECTORY_BASE musi kończyć się na / '))

if GALLERY_DIRECTORY[-1] is not '/':
    raise ImproperlyConfigured(_(u'GALLERY_DIRECTORY musi kończyć się na / '))