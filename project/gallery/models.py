# coding: utf-8

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from filebrowser.fields import FileBrowseField


class Gallery(models.Model):
    owner = models.ForeignKey(User, verbose_name=_(u'owner'))
    name = models.CharField(_(u'name'), max_length=100)
    slug = models.SlugField(_(u'slug'), max_length=100, unique=True)
    description = models.TextField(_(u'description'))

    class Meta:
        ordering = ('-pk',)
        verbose_name = _(u'gallery')
        verbose_name_plural = _(u'galleries')

    def __unicode__(self):
        return self.name


class Image(models.Model):
    gallery = models.ForeignKey(Gallery)
    image = FileBrowseField(_(u'image'), directory=lambda instance, filename: '/'.join([str(instance.gallery.slug), filename]), max_length=100, extensions=['.jpg', '.png', '.gif'], format='image')
    description = models.CharField(_(u'description'), max_length=255)

    class Meta:
        ordering = ('pk',)
        verbose_name = _(u'image')
        verbose_name_plural = _(u'images')

    def __unicode__(self):
        return self.image.name

