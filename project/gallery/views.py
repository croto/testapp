# coding: utf-8

import os
import uuid

from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.core.urlresolvers import reverse_lazy, reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from braces.views import SelectRelatedMixin, StaticContextMixin, FormMessagesMixin

from auth.mixins import LoginRequiredMixin
from .models import Gallery, Image
from .forms import GalleryForm, ImageForm
from .settings import GALLERY_DIRECTORY_BASE, GALLERY_DIRECTORY


class GallerySelectRelatedMixin(SelectRelatedMixin):
    select_related = (u'owner__username',)


class GalleryCreateView(LoginRequiredMixin,
                        GallerySelectRelatedMixin,
                        StaticContextMixin,
                        FormMessagesMixin,
                        CreateView,
                        ):
    model = Gallery
    form_class = GalleryForm
    success_url = reverse_lazy('gallery:user-galleries')
    form_valid_message = _(u'Galeria została dodana.')
    form_invalid_message = _(u'Wypełnij poprawnie formularz.')
    static_context = {
        'header': _(u'Nowa Galeria')
    }

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(GalleryCreateView, self).form_valid(form)


class GalleryListView(GallerySelectRelatedMixin,
                      StaticContextMixin,
                      ListView):
    model = Gallery
    paginate_by = 10
    static_context = {
        'header': _(u'Galerie')
    }


class UserGalleryListView(LoginRequiredMixin,
                          GalleryListView):
    static_context = {
        'header': _(u'Moje Galerie')
    }

    def get_queryset(self):
        return Gallery.objects.select_related().filter(owner=self.request.user)


class GalleryDetailView(LoginRequiredMixin,
                        GallerySelectRelatedMixin,
                        DetailView):
    model = Gallery

    def get_context_data(self, **kwargs):
        context_data = super(GalleryDetailView, self).get_context_data(**kwargs)
        context_data['header'] = self.object.name
        context_data['images'] = Image.objects.filter(gallery=self.object)
        return context_data


class AddImageCreateView(LoginRequiredMixin,
                         FormMessagesMixin,
                         CreateView):
    model = Image
    form_class = ImageForm
    form_valid_message = _(u'Obrazek został dodany do galerii')

    def get_fk_gallery(self):
        return get_object_or_404(Gallery, owner=self.request.user, slug=self.kwargs.get('slug'))

    def get_success_url(self):
        return reverse('gallery:show-gallery', args=[self.gallery.slug])

    def handle_uploaded_file(self, f):
        gallery_dir = os.path.join(GALLERY_DIRECTORY_BASE, GALLERY_DIRECTORY, self.gallery.slug)
        full_file_name = os.path.join(gallery_dir, "%s.%s" % (uuid.uuid4(), f.name.split('.')[-1].lower()))

        try:
            os.makedirs(os.path.join(settings.MEDIA_ROOT, gallery_dir))
        except:
            pass

        with open(os.path.join(settings.MEDIA_ROOT, full_file_name), 'wb+') as dest:
            for chunk in f.chunks():
                dest.write(chunk)

        return full_file_name

    def form_valid(self, form):
        self.gallery = self.get_fk_gallery()
        form.instance.gallery = self.gallery
        form.instance.image = self.handle_uploaded_file(self.request.FILES.get('img'))
        return super(AddImageCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context_data = super(AddImageCreateView, self).get_context_data(**kwargs)
        context_data['gallery'] = self.get_fk_gallery()
        context_data['header'] = _(u'Dodaj obrazek do galerii "%s"') % context_data['gallery']
        return context_data
