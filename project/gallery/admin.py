# coding: utf-8

from django.contrib import admin
from filebrowser.base import FileObject
from filebrowser.settings import ADMIN_THUMBNAIL

from .models import Gallery, Image


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'owner')
    fields = ('name', 'description', 'slug', 'owner')
    readonly_fields = ('slug', 'owner')
    actions = []

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        return []


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('thumb', 'gallery', 'description', 'image')
    readonly_fields = ('image', 'gallery')
    fields = ('gallery', 'image', 'description')
    search_fields = ('gallery__name',)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        return []

    def thumb(self, obj=None):
        if obj.image:
            image = FileObject(obj.image.path)
            if image.filetype == "Image":
                return '<img src="%s" />' % image.version_generate(ADMIN_THUMBNAIL).url
        else:
            return ""
    thumb.allow_tags = True
    thumb.short_description = ''




