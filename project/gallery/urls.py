# coding: utf-8

from django.conf.urls import url

from gallery import views


urlpatterns = [
    url(r'^$', views.GalleryListView.as_view(), name='all-galleries'),
    url(r'^my-gallery/$', views.UserGalleryListView.as_view(), name='user-galleries'),
    url(r'^gallery/(?P<slug>[\w\-_]+)/$', views.GalleryDetailView.as_view(), name='show-gallery'),
    url(r'^gallery/(?P<slug>[\w\-_]+)/add-image/', views.AddImageCreateView.as_view(), name='add-image-to-gallery'),
    url(r'^add/$', views.GalleryCreateView.as_view(), name='add_gallery'),
]

