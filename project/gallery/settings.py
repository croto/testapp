# coding: utf-8

from django.conf import settings


GALLERY_DIRECTORY_BASE = getattr(settings, 'GALLERY_DIRECTORY_BASE', 'uploads/')
GALLERY_DIRECTORY = getattr(settings, 'GALLERY_DIRECTORY', 'gallery/')
