# coding: utf-8

import six

from django.core.exceptions import ImproperlyConfigured
from django.utils.encoding import force_text
from django.utils.functional import Promise
from django.utils.translation import ugettext_lazy as _

from braces import views


class LoginRequiredMixin(views.LoginRequiredMixin, views.MessageMixin):

    permission_denied_message = _(u'Aby obejrzeć stronę należy się zalogować.')

    def get_permission_denied_message(self):

        if self.permission_denied_message is None:
            raise ImproperlyConfigured(
                '{0}.permission_denied_message is not set. Define '
                '{0}.permission_denied_message, or override '
                '{0}.get_permission_denied_message().'.format(self.__class__.__name__)
            )

        if not isinstance(self.permission_denied_message,
                          (six.string_types, six.text_type, Promise)):
            raise ImproperlyConfigured(
                '{0}.form_valid_message must be a str or unicode '
                'object.'.format(self.__class__.__name__)
            )

        return force_text(self.permission_denied_message)

    def no_permissions_fail(self, request=None):
        self.messages.info(self.get_permission_denied_message(), fail_silently=True)
        return super(LoginRequiredMixin, self).no_permissions_fail(request)
