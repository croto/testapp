"""Development settings and globals."""

from .base import *


########## DEBUG CONFIGURATION
DEBUG = True
########## END DEBUG CONFIGURATION


########## TEMPLATE CONFIGURATION
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
########## END TEMPLATE CONFIGURATION


########## MANAGER CONFIGURATION
ADMINS = ()
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION


########## DATABASE CONFIGURATION
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
########## END DATABASE CONFIGURATION


########## TOOLBAR CONFIGURATION
# See: http://django-debug-toolbar.readthedocs.org/en/latest/installation.html#explicit-setup
INSTALLED_APPS += (
    'debug_toolbar',
)
MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)
DEBUG_TOOLBAR_PATCH_SETTINGS = False
INTERNAL_IPS = ('127.0.0.1',)
########## END TOOLBAR CONFIGURATION
