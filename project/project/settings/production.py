"""Production settings and globals."""

from .base import *


########## MANAGER CONFIGURATION
ADMINS = ()
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION


########## DATABASE CONFIGURATION
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
########## END DATABASE CONFIGURATION


########## SITE CONFIGURATION
ALLOWED_HOSTS = []
########## END SITE CONFIGURATION
