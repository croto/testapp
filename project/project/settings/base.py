"""Common settings and globals."""

import os
from random import choice


def gen_secret_key(l):
    """Generate a random secret key of length l."""
    return ''.join(
        [choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(l)]
    )


########## PATH CONFIGURATION
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SITE_ROOT = os.path.dirname(BASE_DIR)
SITE_NAME = os.path.basename(BASE_DIR)
DEPLOY_DIR = os.path.normpath(os.path.join(SITE_ROOT, '../deploy'))
SECRET_FILE = os.path.normpath(os.path.join(DEPLOY_DIR, 'SECRET_KEY'))
########## END PATH CONFIGURATION


########## DEBUG CONFIGURATION
DEBUG = False
########## END DEBUG CONFIGURATION


########## TEMPLATE CONFIGURATION
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(SITE_ROOT, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "django.core.context_processors.i18n",
                "django.core.context_processors.media",
                "django.core.context_processors.static",
                "django.core.context_processors.tz",
            ],
        },
    },
]
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
########## END TEMPLATE CONFIGURATION


########## MANAGER CONFIGURATION
ADMINS = ()
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION


########## DATABASE CONFIGURATION
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
########## END DATABASE CONFIGURATION


########## GENERAL CONFIGURATION
LANGUAGE_CODE = 'pl'
TIME_ZONE = 'Europe/Warsaw'
USE_I18N = True
USE_L10N = True
USE_TZ = True
########## END GENERAL CONFIGURATION


########## MEDIA CONFIGURATION
MEDIA_ROOT = os.path.normpath(os.path.join(DEPLOY_DIR, 'media'))
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION


########## STATIC FILE CONFIGURATION
STATIC_ROOT = os.path.normpath(os.path.join(DEPLOY_DIR, 'assets'))
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.normpath(os.path.join(SITE_ROOT, 'static')),
]
########## END STATIC FILE CONFIGURATION


########## SECRET CONFIGURATION
try:
    SECRET_KEY = open(SECRET_FILE).read().strip()
except IOError:
    try:
        open(SECRET_FILE, 'w').write(gen_secret_key(50))
    except IOError:
        raise Exception('Cannot open file `%s` for writing.' % SECRET_FILE)
########## END SECRET CONFIGURATION


########## SITE CONFIGURATION
ALLOWED_HOSTS = []
########## END SITE CONFIGURATION


########## MIDDLEWARE CONFIGURATION
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)
########## END MIDDLEWARE CONFIGURATION


########## APP CONFIGURATION
BEFORE_DJANGO_APPS = (
    'django.contrib.contenttypes',
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
)
DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)
THIRD_PARTY_APPS = (
    'bootstrap3',
    'sorl.thumbnail',
)
LOCAL_APPS = (
    'admin',
    'gallery',
)
INSTALLED_APPS = BEFORE_DJANGO_APPS + DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
########## END APP CONFIGURATION


########## WSGI CONFIGURATION
WSGI_APPLICATION = '%s.wsgi.application' % SITE_NAME
########## END WSGI CONFIGURATION


########## URL CONFIGURATION
ROOT_URLCONF = '%s.urls' % SITE_NAME
########## END URL CONFIGURATION


########## GRAPPELLI CONFIGURATION
GRAPPELLI_ADMIN_TITLE = 'MASTER PANEL'
GRAPPELLI_INDEX_DASHBOARD = 'admin.dashboard.CustomIndexDashboard'
########## END GRAPPELLI CONFIGURATION


########## FILEBROWSER CONFIGURATION
FILEBROWSER_DIRECTORY = 'uploads/'
########## END FILEBROWSER CONFIGURATION


########## AUTH CONFIGURATION
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
########## END AUTH CONFIGURATION

