# coding: utf-8

from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'admin'
    verbose_name = 'Admin'
    label = 'custom_admin'
