# coding: utf-8

from django.contrib import admin


class PrimaryModelAdmin(admin.ModelAdmin):
    """
    Klasa definiuje wspólny wygląd dla Panelu Admina dla pozostałych aplikacji.
    Zdefiniowane zostały te elementy, które w ten sposób muszą wyglądać na każdej podstronie PA.

    Jednak te elementy nie wykraczają poza standardowy zbiór tego co oferuje samo Django w swoim PA. Rozszerzania
     w przypadku dostosowania PA za pomocą innej aplikacji należy dokonywać w klasach potomnych.
    """
    actions_on_top = False
    actions_on_bottom = True
    list_per_page = 25
    list_select_related = True


class ModelAdmin(PrimaryModelAdmin):
    """
    Czysta klasa PA zaopatrzona o elementy domyślne wspólne dla całego interfejsu.

    W przypadku konieczności dostosowania lub rozszerzenia PA o elementy z innych bibliotek czy aplikacji należy
     utworzyć klasy bazujące na tej klasie lub ją rozszerzyć.
    """
    pass
