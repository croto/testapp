FROM python:2.7

RUN mkdir -p /app/src
RUN mkdir -p /app/deploy

COPY requirements.txt /app/src/requirements.txt
COPY requirements /app/src/requirements

WORKDIR /app/src
RUN pip install --no-cache-dir -r requirements.txt

CMD python manage.py check
